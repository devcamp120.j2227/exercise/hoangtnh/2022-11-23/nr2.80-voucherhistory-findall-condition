//import thư viện express js
const express = require("express");

//khai báo router
const router = express.Router();

//import voucher controller
const voucherController = require("../controllers/voucherController");

router.post("/vouchers", voucherController.createVoucher);
router.get("/vouchers", voucherController.getAllVouchers);
router.get("/vouchers/:voucherId", voucherController.getVoucherById);
router.put("/vouchers/:voucherId", voucherController.updateVoucherById);
router.delete("/vouchers/:voucherId", voucherController.deleteVoucherById);

module.exports = router;