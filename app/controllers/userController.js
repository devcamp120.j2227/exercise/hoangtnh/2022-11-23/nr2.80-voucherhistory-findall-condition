//khai báo thư viện mongoose
const mongoose = require("mongoose");
const { update } = require("../models/userModel");
//import user model
const userModel = require("../models/userModel");

//function create user
const createUser = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    //B2: validate dữ liệu
    if(!body.username){
        return response.status(400).json({
            status: "Bad Request",
            message:"user name không hợp lệ"
        })
    }
    if(!body.firstname){
        return response.status(400).json({
            status: "Bad Request",
            message:"first name không hợp lệ"
        })
    }
    if(!body.lastname){
        return response.status(400).json({
            status: "Bad Request",
            message:"last name không hợp lệ"
        })
    }
    //B3: gọi modelt ạo dữ liệu
    const newUser = {
        _id: mongoose.Types.ObjectId(),
        username: body.username,
        firstname: body.firstname,
        lastname:body.lastname,
        createAt: body.createAt,
        updateAt: body.updatedAt
    }
    userModel.create(newUser, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new user successfully",
            data: data
        })
    })
}

//function get all user
const getAllUser = (request, response) => {
    userModel.find((error, data) =>{
        if(error){
            response.status(400).json({
                status:"Bad request",
                message: error.message
            })
        }
        return response.status(200).json({
            message:"Get all users successfully",
            data: data
        })
    })
}
//function get user by id
const getUserById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const userId = request.params.userId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
            status:"Bad request",
            message:"user id không hợp lệ"
        })
    }
    //B3: gọi model chứa id cần tìm
    userModel.findById(userId, (error,data) => {
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            message:"Get user successfully",
            data: data
        })
    })
}
//function update user by id
const updateUserById = (request, response) => {
    //B1: chuẩn bị dữ liệu'
    const userId = request.params.userId;
    const body = request.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
            status: "Bad request",
            message: "User Id không đúng"
        })
    }
    if(!body.username){
        return response.status(400).json({
            status: "Bad Request",
            message:"user name không hợp lệ"
        })
    }
    if(!body.firstname){
        return response.status(400).json({
            status: "Bad Request",
            message:"first name không hợp lệ"
        })
    }
    if(!body.lastname){
        return response.status(400).json({
            status: "Bad Request",
            message:"last name không hợp lệ"
        })
    }
    //B3: gọi model chưa id để tìm và update dữ liệu
    const updateUser = {};
    if(body.username !== undefined){
        updateUser.username = body.username
    }
    if(body.firstname !== undefined){
        updateUser.firstname = body.firstname
    }
    if(body.lastname !== undefined){
        updateUser.lastname = body.lastname
    }
    userModel.findByIdAndUpdate(userId, updateUser, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Update user successfully",
            data: data
        })
    })
}
//function delete user by id
const deleteUserById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const userId = request.params.userId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
            status:" Bad request",
            message:"User id không đúng"
        })
    }
    //B3: gọi user model chưa id cần xóa
    userModel.findByIdAndRemove(userId,(error, data) => {
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`delete user had id ${userId} successfully`
        })
    })
}
module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}