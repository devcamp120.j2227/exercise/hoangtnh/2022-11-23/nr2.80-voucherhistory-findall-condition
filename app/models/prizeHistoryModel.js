//import thư viện mongoose
const mongoose = require("mongoose");
//import class schema từ thư viện mongoose
const Schema = mongoose.Schema;

//tạo class prize history Schema 
const prizeHistorySchema = new Schema ({
    user: {
        type: mongoose.Types.ObjectId, 
        ref: "User", 
        required: true
    },
	prize: {
        type: mongoose.Types.ObjectId,
        ref: "Prize",
        required: true
    },
	createdAt: {
        type: Date, 
        default: Date.now()
    },
    updatedAt: {
        type: Date, 
        default: Date.now()
    }
});
module.exports = mongoose.model("PrizeHistory", prizeHistorySchema);