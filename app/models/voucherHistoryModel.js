//import thư viện mongoose
const mongoose = require("mongoose");
//import thư viện schema class
const Schema = mongoose.Schema;

//tạo class voucher history schema
const voucherHistorySchema = new Schema ({
    user: {
        type: mongoose.Types.ObjectId, 
        ref: "User", 
        required: true
    },
	voucher: {
        type: mongoose.Types.ObjectId,
        ref: "Voucher",
        required: true
    },
    createdAt: {
        type: Date, 
        default: Date.now()
    },
    updatedAt: {
        type: Date, 
        default: Date.now()
    }
});
module.exports = mongoose.model("VoucherHistory", voucherHistorySchema);